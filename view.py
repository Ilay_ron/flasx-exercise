from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route('/')
@app.route('/home')
@app.route('/index')
def index():
    return render_template('index.html', title="Welcome")

@app.route('/contact')
def contact():
    return render_template('contact.html', title="Contacts")

@app.route('/about')
def about():
    return render_template('about.html', title="About")

app.run(port=8080)
